﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageScreen : MonoBehaviour
{
    public GameObject messageBox;

    public void setMessage(string message)
    {
        if(messageBox != null)
        {
            messageBox.GetComponent<Text>().text = message;
            Debug.Log(message);
        }
    }

}
