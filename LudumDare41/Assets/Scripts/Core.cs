﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Core : MonoBehaviour
{
    private KeyCode HotKey_screenshot = KeyCode.F12;
    public Canvas menu;
    
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "main")
        {
            Cursor.visible = false;
            Pause(false);
            GetComponent<LevelGenerator>().Generate(42);
            GetComponent<LevelGenerator>().Display();
        }
        else if (SceneManager.GetActiveScene().name == "start")
        {

            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Locked; //centre cursor
            Cursor.lockState = CursorLockMode.None;
        }
    }

    
    void Update()
    {
        if (Input.GetKeyDown(HotKey_screenshot))
        {
            Screenshot();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (SceneManager.GetActiveScene().name == "main")
            {
                Pause(true);


            }
            else if (SceneManager.GetActiveScene().name == "start")
            {
                Quit();
            }
        }
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void Screenshot()
    {
        //TODO: handle filename increment
        ScreenCapture.CaptureScreenshot("Screenshot.png");
        // With no directory/folder list the image will be written into the Project folder
    }

    public void Pause(bool pause)
    {

        if (pause == true)
        {
            Time.timeScale = 0;
            AudioListener.pause = true;

            //Update() continues 
            //FixedUpdate() stops being called.The physics engine paused.
            if (menu != null)
            {
                 menu.GetComponent<Canvas>().enabled = true;

                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.Locked; //centre cursor
                Cursor.lockState = CursorLockMode.None;
            }
            

        }
        else
        {
            Time.timeScale = 1;
            AudioListener.pause = false;
            if (menu != null)
            {
                menu.GetComponent<Canvas>().enabled = false;
                Cursor.visible = false;
            }



        }
    }

    public void StartGame()
    {
        Cursor.visible = false;
        SceneManager.LoadSceneAsync("main");
    }

    public void StartMenu()
    {

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Locked; //centre cursor
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadSceneAsync("start");
    }

}
