﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;


public class LevelGenerator : MonoBehaviour
{
    int maxIterations = 20; //test this until we find a good value

    public float tileSize = 3; // effectively the smallest possible room, this is based on door width
    public int levelSize = 4; //number of regions per side
    public int regionSize = 30; //size of regions in tiles per side
    public int iterations = 5;
    Region[,] regions;
    List<Region> outside = new List<Region>();
    List<Vector3> spawnPoints;
    Vector2 centreRegion;

    float wallThickness = 0.1f;
    float wallHeight = 3;

    public Material floorMaterial;
    public Material wallMaterial;
    public Material doorMaterial;
    public Material sectorDoorMaterial;
    public Transform doorPrefab;
    public Transform player;
    public Transform goal; //The Placeholder Object of Destiny

    bool gameStarted;
    bool gameFinished;
    public float countdownReset = 50;
    float countdown;
    public int zoneRadius = 10;
    public int zoneSpeed = 2;
    public Material safeMaterial;
    public Material dangerMaterial;
    public Material outMaterial;
    public Canvas endGameScreen;

    void Start()
    {
        bool gameStarted = false;
        bool gameFinished = false;
    }

    void FixedUpdate()
    {
        if (player.GetComponent<Player>().isWinner) gameFinished = true;
        if(gameStarted)
        {
            //countdown
            countdown--;

            if(countdown < 0)
            {
                
                if(outside.Count == 0)
                {
                    //start a new danger zone
                    //add regions at edge
                    PutRegionsInDangerZone(zoneRadius);
                    zoneRadius -= zoneSpeed;
                    //reset timer
                    countdown = countdownReset;
                }
                else
                {
                    //start shutting down sectors outside the safe zone
                    int index = Random.Range(0, outside.Count - 1);
                    Region target = outside[index]; // get random sector
                    //outside.RemoveAt(index); //remove from list
                    StartCoroutine(SectorShutDown(target));

                    

                }
            }

        }

        if(gameFinished)
        {
            Debug.Log("ended");
            //Hide player HUD
            player.GetComponent<Player>().ShowHUD(false);

            //show end screen
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Locked; //centre cursor
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0;
            AudioListener.pause = true;
            endGameScreen.GetComponent<Canvas>().enabled = true;
            if(player.GetComponent<Player>().isWinner)
            {
                endGameScreen.GetComponent<MessageScreen>().setMessage("Winner");
                Debug.Log("Winner");
            }
            else
            {
                endGameScreen.GetComponent<MessageScreen>().setMessage("Loser");
                Debug.Log("Loser");
            }


        }
    }

    void PutRegionsInDangerZone(int radius)
    {
        for (int x = 0; x < regions.GetLength(0); x++)
        {
            for (int y = 0; y < regions.GetLength(1); y++)
            {
                if( Vector2.Distance(centreRegion,new Vector2(x,y))> radius && regions[x, y].isOpen)
                {
                    outside.Add(regions[x, y]);
                    regions[x, y].mapRegion.GetComponent<MeshRenderer>().material = dangerMaterial;
                }
                
            }
        }
    }

    IEnumerator SectorShutDown(Region target)
    {
        int delay = 10;
        foreach(Door door in target.doors)
        {
            //if(delay == 0)
                door.Close();
            delay--;
            yield return new WaitForFixedUpdate();
            
        }

        outside.Remove(target);
        target.isOpen = false;
        target.mapRegion.GetComponent<MeshRenderer>().material = outMaterial;
        float x1 = target.offsetX * regionSize * tileSize;
        float x2 = x1 + regionSize * tileSize;

        float y1 = target.offsetY * regionSize * tileSize;
        float y2 = y1 + regionSize * tileSize;
        Vector3 p = player.transform.position;

        //check player location. for lose condition 
        if (p.x > x1 && p.x <x2 && p.z >y1 && p.z <y2)
        {
            //player loses
            player.GetComponent<Player>().isWinner = false;
            gameFinished = true;
           gameStarted = false;
        }

        //reset timer
        countdown = countdownReset;
    }

    public void Generate(int seed)
    {
        regions = new Region[levelSize,levelSize];
        spawnPoints = new List<Vector3>();

        //generate regions
        for (int x = 0; x < regions.GetLength(0); x++)
        {
            for (int y = 0; y < regions.GetLength(1); y++)
            {
                regions[x, y] = new Region(x,y);
                regions[x, y].Generate(regionSize, iterations);
            }
        }

        //connect regions
        for (int x = 0; x < regions.GetLength(0); x++)
        {

            for (int y = 0; y < regions.GetLength(1); y++)
            {
                if(x > 0)
                {
                    regions[x,y].ConnectRegion(regions[x - 1, y], new Vector2(-1,0));
                }

                if (x < regions.GetLength(0)-1)
                {
                    regions[x, y].ConnectRegion(regions[x + 1, y], new Vector2(1, 0));

                }

                if (y > 0)
                {
                    regions[x, y].ConnectRegion(regions[x, y - 1], new Vector2(0, -1));
                }


                if (y < regions.GetLength(1) - 1)
                {
                    regions[x, y].ConnectRegion(regions[x, y + 1], new Vector2(0, 1));
                }
            }
        }
    }

    public void Display()
    {
        List<int> regionDoorIndices;
        for (int x = 0; x < regions.GetLength(0); x++)
        {
            for (int y = 0; y < regions.GetLength(1); y++)
            {
                regionDoorIndices = BuildRegion(regions[x, y], regionSize * x, regionSize * y);

                for (int i = 0; i < regionDoorIndices.Count; i++)
                {
                    //check for shared doors between regions
                    int rIndex = regionDoorIndices[i];
                    Door regionDoor = regions[x, y].doors[rIndex];
                    if (x > 0)
                    {
                        int index = regions[x - 1, y].doors.FindIndex(d => d.transform == regionDoor.transform);
                        if ( index != -1)
                        {
                            regions[x, y].doors[rIndex] = regions[x - 1, y].doors[index];
                            Destroy(regionDoor.gameObject);
                        }
                    }

                    if (x < regions.GetLength(0) - 1)
                    {
                        
                        int index = regions[x + 1, y].doors.FindIndex(d => d.transform == regionDoor.transform);
                        if (index != -1)
                        {
                            regions[x, y].doors[rIndex] = regions[x + 1, y].doors[index];
                            Destroy(regionDoor.gameObject);
                        }

                    }

                    if (y > 0)
                    {
                        int index = regions[x, y - 1].doors.FindIndex(d => d.transform == regionDoor.transform);
                        if (index != -1)
                        {
                            regions[x, y].doors[rIndex] = regions[x, y - 1].doors[index];
                            Destroy(regionDoor.gameObject);
                        }
                    }


                    if (y < regions.GetLength(1) - 1)
                    {
                        int index = regions[x, y + 1].doors.FindIndex(d => d.transform == regionDoor.transform);
                        if (index != -1)
                        {
                            regions[x, y].doors[rIndex] = regions[x, y + 1].doors[index];
                            Destroy(regionDoor.gameObject);
                        }
                    }
                }
            }
        }

        int spawnPointIndex = Random.Range(0,spawnPoints.Count-1);

        player.position = spawnPoints[spawnPointIndex];

        centreRegion = new Vector2(Random.Range(0, levelSize - 1), Random.Range(0, levelSize - 1));
        countdown = countdownReset;
        gameStarted = true;
        AudioListener.pause = false;

        Region endRegion = regions[(int)centreRegion.x,(int) centreRegion.y];
        goal.position =
        new Vector3(
            (endRegion.rooms[0].corner.x + (float)endRegion.rooms[0].width / 2f + regionSize * centreRegion.x) * tileSize
            , 1f,
            (endRegion.rooms[0].corner.y + (float)endRegion.rooms[0].length / 2f + regionSize * centreRegion.y) * tileSize
            );

    }


    //this is a spaghetti nightmare  - needs refactoring desperately
    List<int> BuildRegion(Region region,int regionOffsetX, int regionOffsetY)
    {
        //for minimap
        GameObject regionArea = GameObject.CreatePrimitive(PrimitiveType.Cube);
        regionArea.transform.localScale = new Vector3(regionSize * tileSize, 0.1f, regionSize * tileSize);
        regionArea.transform.position = new Vector3(regionOffsetX * tileSize + regionSize * tileSize/2, -1, regionOffsetY * tileSize + regionSize * tileSize / 2);
        regionArea.layer = 8;
        regionArea.name = "MapSquare";
        regionArea.GetComponent<MeshRenderer>().material = safeMaterial;
        region.mapRegion = regionArea;

        List<int> regionDoorIndices = new List<int>();
        foreach (Room room in region.rooms)
        {
            HashSet<Vector3> doorsX = new HashSet<Vector3>();
            HashSet<Vector3> doorsY = new HashSet<Vector3>();
            HashSet<Vector3> regionDoorsX = new HashSet<Vector3>();
            HashSet<Vector3> regionDoorsY = new HashSet<Vector3>();
            

            float halfWallThickness = wallThickness / 2f;
            float halfRoomWidth = (room.width * tileSize) / 2f;
            float halfRoomLength = (room.length * tileSize) / 2f;
            float halfRoomHeight = wallHeight / 2f;
            if (room.connectedRooms.Count != 0)
            {
                   //TODO: add a spawn point
                Vector3 floorCentre = BuildRoom(room, regionOffsetX, regionOffsetY);

                spawnPoints.Add(new Vector3(floorCentre.x, floorCentre.y + 1.25f,floorCentre.z));

                GameObject wall;
                float ypos = floorCentre.y + halfRoomHeight;
                //horizontal walls
                for (int x = room.corner.x; x < room.corner.x+room.width; x++)
                {
                    Vector3 xscale = new Vector3(tileSize, wallHeight, wallThickness);
                    float xpos = (regionOffsetX + x) * tileSize + tileSize / 2f;
                    if ( region.tiles[x, room.corner.y]< 2 )
                    {
                       
                        wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        wall.transform.localScale = xscale;
                        wall.transform.position = new Vector3(xpos, ypos, floorCentre.z - halfRoomLength + halfWallThickness);
                        if (wallMaterial != null)
                        { wall.GetComponent<MeshRenderer>().material = wallMaterial; }

                    }
                    else if (region.tiles[x, room.corner.y] == 2) //this is a doorway
                    {
                        doorsX.Add(new Vector3(xpos, ypos, floorCentre.z - halfRoomLength));
                    }
                    else if (region.tiles[x, room.corner.y] == 3)
                    {
                        //this is a region connecting door
                        regionDoorsX.Add(new Vector3(xpos, ypos, floorCentre.z - halfRoomLength));

                    }


                    if (region.tiles[x, room.corner.y + room.length-1] < 2)
                    {

                        wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        wall.transform.localScale = xscale;
                        wall.transform.position = new Vector3(xpos, ypos, floorCentre.z + halfRoomLength - halfWallThickness);
                        if (wallMaterial != null)
                        { wall.GetComponent<MeshRenderer>().material = wallMaterial; }

                    }
                    else if(region.tiles[x, room.corner.y + room.length - 1] == 2)
                    {
                        doorsX.Add(new Vector3(xpos, ypos, floorCentre.z + halfRoomLength));

                    }
                    else if (region.tiles[x, room.corner.y + room.length - 1] == 3)
                    {
                        //this is a region connecting door
                        regionDoorsX.Add(new Vector3(xpos, ypos, floorCentre.z + halfRoomLength));
                    }
                }

                //vertical walls
                for (int y = room.corner.y; y < room.corner.y + room.length; y++)
                {
                    Vector3 zscale = new Vector3(wallThickness, wallHeight,tileSize );
                    float zpos = (regionOffsetY + y) * tileSize + tileSize / 2f;

                    if (region.tiles[room.corner.x, y] < 2)
                    {

                        wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        wall.transform.localScale = zscale;
                        wall.transform.position = new Vector3(floorCentre.x - halfRoomWidth + halfWallThickness, ypos, zpos);
                        if (wallMaterial != null)
                        { wall.GetComponent<MeshRenderer>().material = wallMaterial; }

                    }
                    else if (region.tiles[room.corner.x, y]== 2)//this is a doorway
                    {
                        doorsY.Add(new Vector3(floorCentre.x - halfRoomWidth, ypos, zpos));
                    }
                    else if (region.tiles[room.corner.x, y] ==3)
                    {
                        //this is a region connecting door
                        regionDoorsY.Add(new Vector3(floorCentre.x - halfRoomWidth, ypos, zpos));

                    }

                    if (region.tiles[room.corner.x + room.width-1, y] < 2)
                    {

                        wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        wall.transform.localScale = zscale;
                        wall.transform.position = new Vector3(floorCentre.x + halfRoomWidth - halfWallThickness, ypos, zpos);
                        if (wallMaterial != null)
                        { wall.GetComponent<MeshRenderer>().material = wallMaterial; }

                    }
                    else if(region.tiles[room.corner.x + room.width - 1, y] == 2)//this is a doorway
                    {
                        doorsY.Add(new Vector3(floorCentre.x + halfRoomWidth, ypos, zpos));


                    }
                    else if (region.tiles[room.corner.x + room.width - 1, y] == 3)//this is a sector doorway
                    {
                        //this is a region connecting door
                        regionDoorsY.Add(new Vector3(floorCentre.x + halfRoomWidth, ypos, zpos));

                    }
                }

                //hacky test of doors
                foreach(Vector3 doorPos in doorsX)
                {
                    Transform door = GameObject.Instantiate(doorPrefab, doorPos, Quaternion.identity);
                    door.GetComponent<Door>().SetMaterials(doorMaterial, wallMaterial);
                    region.doors.Add(door.GetComponent<Door>());

                }

                foreach (Vector3 doorPos in doorsY)
                {
                    Transform door = GameObject.Instantiate(doorPrefab, doorPos, Quaternion.Euler(new Vector3(0,90,0)));
                    door.GetComponent<Door>().SetMaterials(doorMaterial, wallMaterial);
                    region.doors.Add(door.GetComponent<Door>());



                }
                

                foreach (Vector3 doorPos in regionDoorsX)
                {
                    Transform door = GameObject.Instantiate(doorPrefab, doorPos, Quaternion.identity);
                    door.GetComponent<Door>().SetMaterials(sectorDoorMaterial, wallMaterial);
                    region.doors.Add(door.GetComponent<Door>());
                    regionDoorIndices.Add(region.doors.Count - 1);
                }

                foreach (Vector3 doorPos in regionDoorsY)
                {
                    Transform door = GameObject.Instantiate(doorPrefab, doorPos, Quaternion.Euler(new Vector3(0, 90, 0)));
                    door.GetComponent<Door>().SetMaterials(sectorDoorMaterial, wallMaterial);
                    region.doors.Add(door.GetComponent<Door>());
                    regionDoorIndices.Add(region.doors.Count - 1);
                }

                foreach (Door door in region.doors)
                {
                    
                    door.Open();

                }

            }

            
        }

        return regionDoorIndices;


    }

    Vector3 BuildRoom(Room room, int regionOffsetX, int regionOffsetY)
    {
        GameObject floor = GameObject.CreatePrimitive(PrimitiveType.Cube);
        
        floor.transform.localScale = new Vector3(room.width * tileSize, 1, room.length * tileSize);
        floor.transform.position = new Vector3(
            (room.corner.x + (float)room.width / 2f + regionOffsetX) * tileSize
            , 0,
            (room.corner.y + (float)room.length / 2f + regionOffsetY) * tileSize
            );

        GameObject ceiling = GameObject.CreatePrimitive(PrimitiveType.Cube);
        ceiling.transform.localScale = floor.transform.localScale;
        ceiling.transform.position = new Vector3((room.corner.x + (float)room.width / 2f + regionOffsetX) * tileSize
            , wallHeight +0.5f,
            (room.corner.y + (float)room.length / 2f + regionOffsetY) * tileSize
            );
        ceiling.layer = 9;
        floor.layer = 9;

        if (floorMaterial != null)
        { floor.GetComponent<MeshRenderer>().material = floorMaterial;}

        return floor.transform.position;
     

    }

    private void OnValidate()
    {
        if (tileSize < 1) tileSize = 1;
        if (levelSize < 1) levelSize = 1;
        if (regionSize < 4) regionSize = 4;
        if (iterations < 1) regionSize = 1;
        if (iterations > maxIterations) iterations = maxIterations;

    }
    

    public class Region
    {
        public int offsetX = 0;
        public int offsetY = 0;
        public bool isOpen = true;
        public GameObject mapRegion; //represents region on minimap
        public List<Room> rooms;
        public List<Door> doors;//doors attached to this regin. this includes shared doors of other regions
        public List<Region> connectedRegions;
        public int[,] tiles; // 1 = edge, 0 = floor, 2 = door
        public void Generate(int size, int iterations)
        {
            connectedRegions = new List<Region>();
            rooms = new List<Room>();
            doors = new List<Door>();
            Room initial = new Room(new Tile(0, 0), size, size);
            rooms = initial.Split(iterations);


            tiles = new int[size, size];
            foreach (Room room in rooms)
            {

                for (int x = room.corner.x; x < room.corner.x+room.width; x++)
                {
                    for (int y = room.corner.y; y < room.corner.y+room.length; y++)
                    {
                        if ( //room edges but not corners
                            (x == room.corner.x || x == room.corner.x + room.width - 1) && (y != room.corner.y && y != room.corner.y+room.length - 1)
                            || (x != room.corner.x && x != room.corner.x + room.width - 1) && (y == room.corner.y || y == room.corner.y + room.length - 1)
                            )
                        {
                            tiles[x, y] = 1;
                            room.wallAdjacentTiles.Add(new Tile(x, y));
                        }
                        else
                        {
                            tiles[x, y] = 0;
                        }

                    }
                }
            }

            //find adjacent rooms
            foreach (Room roomA in rooms)
            {

                foreach (Room roomB in rooms)
                {
                    if (roomA == roomB) continue;
                    foreach (Tile t1 in roomA.wallAdjacentTiles)
                    {
                        foreach (Tile t2 in roomB.wallAdjacentTiles)
                        {
                            if (t1.isAdjacent(t2))
                            {
                                roomA.adjacentRooms.Add(roomB);
                                roomB.adjacentRooms.Add(roomA);
                            }
                        }
                    }
                }
            }

            //connect rooms
            foreach (Room roomA in rooms) // connect to 1 other room not already connected to
            {
                foreach (Room roomB in roomA.adjacentRooms)
                {
                    if(!roomB.connectedRooms.Contains(roomA))
                    {
                        //not already connected lets do it
                        roomA.connectedRooms.Add(roomB);
                        roomB.connectedRooms.Add(roomA);
                        foreach (Tile t1 in roomA.wallAdjacentTiles)
                        {
                            bool doorFound = false;
                            foreach (Tile t2 in roomB.wallAdjacentTiles)
                            {
                                if (t1.isAdjacent(t2))
                                {

                                    tiles[t1.x, t1.y] = 2;
                                    tiles[t2.x, t2.y] = 2;
                                    doorFound = true;
                                    break;
                                }
                            }
                            if (doorFound) break;
                        }
                        break;
                    }
                }

            }



        }
        public Region(int offsetX, int offsetY)
        {
            this.offsetX = offsetX;
            this.offsetY = offsetY;

        }

        public void ConnectRegion(Region other, Vector2 row)
        {
            //row : left = (-1,0) right = (1,0), above = (0,1), below = (0,-1)
            if (other != null && !connectedRegions.Contains(other))
            {
                // axis == 0 means X axis
                List<Tile> myTiles;// = GetRegionEdgeTiles(0, 0);
                List<Tile> otherTiles; //= other.GetRegionEdgeTiles(tiles.GetLength(0) - 1, 0);

                if (row.y == 0)
                {
                    //left or right
                    if(row.x < 0)
                    {
                        myTiles = GetRegionEdgeTiles(0, 0);
                        otherTiles = other.GetRegionEdgeTiles(tiles.GetLength(0) - 1, 0);
                    }
                    else
                    {
                        myTiles = GetRegionEdgeTiles(tiles.GetLength(0) - 1, 0);
                        otherTiles = other.GetRegionEdgeTiles(0, 0);
                    }
                }
                else
                {
                    //above or below
                    if (row.y < 0)
                    {
                        myTiles = GetRegionEdgeTiles(0,1);
                        otherTiles = other.GetRegionEdgeTiles(tiles.GetLength(1) - 1, 1);
                    }
                    else
                    {
                        myTiles = GetRegionEdgeTiles(tiles.GetLength(1) - 1, 1);
                        otherTiles = other.GetRegionEdgeTiles(0, 1);

                    }
                }

                foreach (Tile t1 in myTiles)
                {
                    bool doorFound = false;
                    foreach (Tile t2 in otherTiles)
                    {
                        bool match;//which axis 
                        if (row.y == 0) match = t1.y == t2.y;
                        else match = t1.x == t2.x;

                        if (match) 
                        {
                            tiles[t1.x, t1.y] = 3; 
                            other.tiles[t2.x, t2.y] = 3;


                            doorFound = true;
                            connectedRegions.Add(other);
                            break;

                            /* 
                              Note: this always finds door spot in same location unless a perpendicular wall is there , 
                              as it picks the first available
                              we should collect a list  of valid door locations  and select randomly
                            */
                        }
                    }
                    if (doorFound) break;
                }

            }

        }

        List<Tile> GetRegionEdgeTiles(int i, int axis)
        {
            List<Tile> tiles = new List<Tile>();
            if (axis == 0)
            {
                foreach (Room room in rooms)
                {
                    foreach (Tile tile in room.wallAdjacentTiles)
                    {
                        if (tile.x == i) tiles.Add(tile);
                    }
                }
            }
            else
            {
                foreach (Room room in rooms)
                {
                    foreach (Tile tile in room.wallAdjacentTiles)
                    {
                        if (tile.y == i) tiles.Add(tile);

                    }
                }
            }

            return tiles;
        }

    }

    public class Room
    {
        public Tile corner; //top left corner
       /// public int[,] tiles; // 1 = wall, 0 = floor
        public int width;
        public int length;
        Room left;
        Room right;
        public List<Tile> wallAdjacentTiles = new List<Tile>();
        public List<Room> adjacentRooms = new List<Room>();
        public List<Room> connectedRooms = new List<Room>();

        public Room(Tile corner,int width, int length)
        {
            this.corner = corner;
            this.width = width;
            this.length = length;
            ///tiles = new int[width, length];
        }

        public List<Room> Split(int iterations)
        {
            int minSplittableWallSize = 6;// this leads to too large rooms
                                          //TODO: combine with ratio check so that we can keep splitting long 
                                            //walls when short wall is less than  minSplittableWallSize

            List<Room> rooms = new List<Room>();
            int axis = GetRandomAxis();
            float wallLengthRatio = (float)width / (float)length;
            if(wallLengthRatio > 2)
            {
                //room is too wide & short
                axis = 0;//
            }
            else if(wallLengthRatio < 0.5f)
            {
                //room is too thin & long
                axis = 1;
            }

            if (axis == 0)
            {
                int split = Random.Range(3,  width - 3);
                left = new Room(corner,split, length);
                right = new Room(new Tile(corner.x + split,corner.y), width - split, length);
            }
            else
            {
                int split = Random.Range(3, length - 3);

                left = new Room(corner, width,split);
                right = new Room(new Tile(corner.x, corner.y + split), width, length - split);
            }

            iterations--;
            
            if(iterations>0 )
            {
                if(left.width> minSplittableWallSize && left.length > minSplittableWallSize)
                { rooms.AddRange(left.Split(iterations));}
                else
                { rooms.Add(left); }

                if (right.width > minSplittableWallSize && right.length > minSplittableWallSize)
                {rooms.AddRange(right.Split(iterations)); }
                else
                { rooms.Add(right); }
                    
            }
            else
            {
                rooms.Add(left);
                rooms.Add(right);

            }

            return rooms;
        }

        int GetRandomAxis()
        {
            int n = Random.Range(0, 100);
            if (n > 50) return 1;
            else return 0;
        }
    }



    public struct Tile
    {
        public Tile(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        public int x;
        public int y;

        public bool isAdjacent(Tile t)
        {
            if (((t.x == x-1 || t.x == x+1 )&& y==t.y )|| ((t.y == y - 1 || t.y == y + 1) && x == t.x))
            { return true; }
            else
            { return false; }
        }
    }
}
