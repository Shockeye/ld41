﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour
{
    public bool invertMouse = false;
    public float move;
    public float jump;
    public float mouseSensitivity;
    public Camera playerCamera;
    Rigidbody playerBody;
    float FootstepTime=0;
    public float FootstepDelay = 200;
    public AudioClip footstep; //while moving
    public AudioClip landJump; //when landing
    AudioSource audioSource;
    public Canvas HUD;
    [HideInInspector]
    public bool isWinner;

    void Start ()
    {
        isWinner = false;
        playerBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();

    }
	

    void FixedUpdate()
    {
        playerBody.MoveRotation(playerBody.rotation * Quaternion.Euler(new Vector3(0, Input.GetAxis("Mouse X") * mouseSensitivity, 0)));
        playerCamera.transform.Rotate(new Vector3((invertMouse ? 1 : -1) *Input.GetAxis("Mouse Y") * mouseSensitivity, 0, 0));
        playerBody.MovePosition(transform.position + (transform.forward * Input.GetAxis("Vertical") * move) + (transform.right * Input.GetAxis("Horizontal") * move));
        if (Input.GetKeyDown(KeyCode.Space))
            playerBody.AddForce(transform.up * jump);

        if (Input.GetAxis("Vertical") != 0)
            Footstep();
        
    }
    void Footstep()
    {
        if(FootstepTime < 0)
        {
            audioSource.PlayOneShot(footstep,0.5f);

            FootstepTime = FootstepDelay;
        }
        else { FootstepTime--; }
        
    }

    public void ShowHUD(bool show)
    {
        if(show) HUD.GetComponent<Canvas>().enabled = true;
        else HUD.GetComponent<Canvas>().enabled = true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(this.transform.position, new Vector3(1, 2, 1));
    }
}
