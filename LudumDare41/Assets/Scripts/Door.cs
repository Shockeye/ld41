﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    //door stuff
    public enum DoorState { closed, opening, open, closing };
    public DoorState doorState;
    public float openPositionX; //these values are ABS, apply sign to one door
    public float closedPositionX;
    float currentPositionX;
    public float doorSpeed = 0.01f; //per frame (?)
    public Transform leftDoor;
    public Transform rightDoor;
    public Transform wallAboveDoor; //if there's a special word for this I couldn't find it on Google ... or Bing!

    public AudioClip doorSliding; //while moving
    public AudioClip doorClang; //when finish closing
    AudioSource audioSource;

    //TODO: we could put an AnimationCurve in here so the door mvmt is not linear.

    // Use this for initialization
    void Start()
    {

        audioSource = GetComponent<AudioSource>();
    }

    public void Open()
    {
        StartCoroutine(OpenDoor());
    }

    public void Close()
    {
        StartCoroutine(CloseDoor());
    }

    void OnTriggerEnter(Collider other)
    {

    }

    IEnumerator CloseDoor()
    {
        doorState = DoorState.closing;
        float amount = 0;
        audioSource.volume = 0.025f;
        audioSource.loop = true;
        audioSource.Play();
        while (currentPositionX != closedPositionX)
        {
            amount += doorSpeed;
            MoveDoor(amount, closedPositionX);
            
            yield return new WaitForFixedUpdate();
        }

        doorState = DoorState.closed;
        audioSource.Stop();
        audioSource.loop = false;
        audioSource.PlayOneShot(doorClang);
}

    IEnumerator OpenDoor()
    {
        doorState = DoorState.opening;
        float amount = 0;
        while (currentPositionX != openPositionX)
        {
            amount += doorSpeed;
            MoveDoor(amount, openPositionX);
            yield return new WaitForFixedUpdate();
        }
        doorState = DoorState.open;
    }

    void MoveDoor(float amount, float destination)
    {
        currentPositionX = Mathf.Lerp(leftDoor.localPosition.x, destination, amount);
        leftDoor.localPosition = new Vector3(currentPositionX, leftDoor.localPosition.y, leftDoor.localPosition.z);
        rightDoor.localPosition = new Vector3(-currentPositionX, rightDoor.localPosition.y, rightDoor.localPosition.z);
    }

    public void SetMaterials(Material doorMat, Material wallMat)
    {
        leftDoor.GetComponent<MeshRenderer>().material = doorMat; //door mat ha ha!
        rightDoor.GetComponent<MeshRenderer>().material = doorMat;
        wallAboveDoor.GetComponent<MeshRenderer>().material = wallMat; // not a pun, unfortunately
    }

}
